import java.util.Scanner;
import java.util.Arrays;


public class CodeVer2 {


	public static void main(String[] args) 
	{
		Double num1, num2, num3, num4, num5, sum, avg;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the first number:");
		num1 = keyboard.nextDouble();
		System.out.println("Enter the seond number:");
		num2 = keyboard.nextDouble();
		System.out.println("Enter the third number:");
		num3 = keyboard.nextDouble();
		System.out.println("Enter the fourth number:");
		num4 = keyboard.nextDouble();
		System.out.println("Enter the fifth number:");
		num5 = keyboard.nextDouble();
		keyboard.close();
		
		double[] num = new double[5]; // array named num is declared with 5 variables
		num[0] = num1;
		num[1] = num2;
		num[2] = num3;
		num[3] = num4;
		num[4] = num5;
		Arrays.sort(num);
	
		double[] data = {num1, num2, num3, num4, num5};
		int mode = 0;
		int[] index = new int[9999];
		int maximum = Integer.MIN_VALUE;
		
		for (int i = 0; i < data.length; i++){
			index[(int) data[i]]++;
		}
		for (int i = 0; i < index.length; i++){
			if(maximum < index[i]){
				maximum = (int) index[i];
				mode = i;
			}
		}
		
		sum = num[0] + num[1] + num[2] + num[3] + num[4];
		avg = sum/5;
				
		System.out.println(" ");
		System.out.println("Sum:    " + sum);
		System.out.println("Average:" + avg);
		System.out.println("Maximum:" + num[4]);
		System.out.println("Minimum:" + num[0]);
		System.out.println("Median: " + num[2]);
		System.out.println("Mode:   " + mode);
		
		}
}